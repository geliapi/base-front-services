package com.geli.geliapi.baseservices.service;

import com.geli.geliapi.baseservices.dto.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

/**
 * Open Feign di gunakan untuk call ke service lain,
 * - Jangan hard code host tujuan, cukup isi dengan nama instance / aplikasi yg telah terdaftar di discovery services
 * - Alternatif Open Feign bisa menggunakan RestTemplate
 * - Fallback di excecute ketika service yang di call sedang off / down
 */
@FeignClient(name = "baseendservices", fallback = ExampleServiceFallback.class)
public interface ExampleService {
    @GetMapping("/api/product/")
    public Iterable<Product> dataProduct();

    @GetMapping("/api/host/")
    public Map<String, Object> backendInfo();
}
