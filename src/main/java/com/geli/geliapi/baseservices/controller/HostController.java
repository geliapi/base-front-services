package com.geli.geliapi.baseservices.controller;

import com.geli.geliapi.baseservices.service.ExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
public class HostController {

    @Autowired
    private ExampleService exampleService;

    @GetMapping("/host")
    public Map<String, Object> hostInfo(HttpServletRequest request) {
       return exampleService.backendInfo();
    }
}
