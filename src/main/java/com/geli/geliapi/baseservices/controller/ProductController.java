package com.geli.geliapi.baseservices.controller;

import com.geli.geliapi.baseservices.dto.Product;
import com.geli.geliapi.baseservices.service.ExampleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class ProductController {

    @Autowired private ExampleService exampleService;

    @GetMapping("/product")
    public Iterable<Product> allProducts() {

        log.info("Mengambil data produk ke Backend Service");
        return exampleService.dataProduct();
    }
}
